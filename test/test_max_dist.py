from max_dist import max_dist,InvalidArgument
import pytest

def test_A1():
    with pytest.raises(InvalidArgument):
        assert max_dist() is None

def test_A2():
    with pytest.raises(InvalidArgument):
        assert max_dist([2]) is None

def test_A3():
    with pytest.raises(TypeError):
        assert max_dist(["hola",2,3]) is None

def test_B():
    assert max_dist([2,2]) is 0

def test_B1():
        assert max_dist([4, 1, 3]) is 3

def test_B2():
    assert max_dist([4, 1, 3, 6, 8]) is 3