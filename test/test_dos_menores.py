from dos_menores import dos_menores

def test_A1():
    assert dos_menores() == None

def test_A2():
    assert dos_menores([]) == None

def test_A3():
    assert dos_menores("Tipo string") == None

def test_B():
    assert dos_menores([1]) == (1)

def test_B1():
    assert dos_menores([1,4,5]) == (1,4)

def test_C():
   assert dos_menores([2,1]) == (1,2)

def test_C1():
   assert dos_menores([1,2]) == (1,2)

def test_D():
   assert dos_menores([1,2]) == (1,2)

def test_D1():
   assert dos_menores([2,1,1]) == (1,1)

def test_D2():
   assert dos_menores([2,1,1,-5]) == (-5,1)

def test_E():
   assert dos_menores([2,5,1]) == (1,2)

def test_E1():
   assert dos_menores([2,5,3]) == (2,3)

def test_F():
    assert dos_menores([2, 5, 4]) == (2, 4)

def test_F1():
    assert dos_menores([3, 5, 7]) == (3, 5)


