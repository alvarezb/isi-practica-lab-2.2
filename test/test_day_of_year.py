import pytest
from day_of_year import day_of_year,InvalidArgument

def test_A1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(33,4,2022) is None #Dia Mayor que 31

def test_A2():
    with pytest.raises(InvalidArgument):
        assert day_of_year(-4,4,2022) is None #Dia menor que 1

def test_A3():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,14,2022) is None #Mes mayor que 12

def test_A4():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,-5,2022) is None #Mes menor que 1

def test_A5():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,2,2450) is None #Año mayor que 2022

def test_A6():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,2,-2000) is None #Año mayor que 2022

def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30) is None #Solo 1 argumento

def test_B1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,2) is None #Solo dos argumentos

def test_B2():
    with pytest.raises(InvalidArgument):
        assert day_of_year() is None #Año mayor que 2022

def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year("30",3,2000) is None #Tipo de dia no es un entero

def test_C1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,"2",2000) is None #Tipo de mes no es un entero

def test_C2():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,4,"2040") is None #Tipo de año no es un entero

def test_D():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,2,2000) is None

def test_D1():
    with pytest.raises(InvalidArgument):
        assert day_of_year(30,2,2001) is None

def test_D2():
    with pytest.raises(InvalidArgument):
        assert day_of_year(31,4,2020) is None

def test_D3():
    assert day_of_year(1,3,1984) is 61




