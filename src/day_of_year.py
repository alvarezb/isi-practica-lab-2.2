import datetime
import sys
import test

class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass
def es_bisiesto(año):
	return not año % 4 and (año % 100 or not año % 400)

def day_of_year(day=None,month=None,year=None):
    if not type(day) is int or not type(month) is int or not type(year) is int:
        raise InvalidArgument
    #Si es bisiesto
    if(es_bisiesto(year) and (month == 2 and day > 29)):
        raise InvalidArgument

    if (not es_bisiesto(year) and (month == 2 and day >28)):
        raise InvalidArgument

    if (day > 30 and (month == 4 or month == 6 or month == 9 or month == 11)):
        raise InvalidArgument

    if (day > 31 or day < 1 or month < 1 or month > 12 or year > 2022 or year <0 or day == None or month == None or year == None):
        raise InvalidArgument

    date = datetime.date(year,month,day)
    day = date.strftime("%j")
    return int(day[1:])
